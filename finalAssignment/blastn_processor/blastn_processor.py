#!/usr/bin/env python3
""""
Name: blastn_processor.py
Purpose: Processing a genbank file and calculating possible oligo nucleotides
Commandline usage: genbank_parser.py [-h] -d <path to genbank file or directory>
Author: Sven Hakvoort & Kimberley Kalkhoven
Created: 20/12/2016
Version: 1.0
"""
# Imports
import os
import mysql.connector
import time
import configparser
from multiprocessing import Pool

class Blastprocessor:
    """Class for processing blast results and determining which oligos are valid as a probe"""

    def __init__(self):
        self.path = ''
        self.conn = None
        self.probes = []
        self.one_hit = []
        self.total = 0
        self.dict_probe_sequence = {}
        self.parse_cli()

    def parse_cli(self):
        """
        Function for parsing the command line arguments
        """

        # self.conn = mysql.connector.connect(host=snakemake.config["database_host"],
        #                                     database=snakemake.config["database_name"],
        #                                     user=snakemake.config["database_username"],
        #                                     password=snakemake.config["database_password"])

        p = Pool(snakemake.threads)
        p.map(self.one_hit_blast_results, snakemake.input)
        #self.one_hit_blast_results(snakemake.input)

    # def get_files(self):
    #     """Function for getting all the file names in a dir, determines if
    #     self.filename is a file or a directory. If it is a directory it lists
    #     all the files and calls the self.read_file function for every file.
    #     If self.filename is a single file it calls self.read_file just once"""
    #     # Join the path to get the full path to the file
    #     if os.path.isfile(os.path.join(os.getcwd(), self.path)):
    #         self.filter_and_sort_blast_results(os.path.join(os.getcwd(), self.path))
    #     elif os.path.isdir(os.path.join(os.getcwd(), self.path)):
    #         for filename in os.listdir(self.path):
    #             self.filter_and_sort_blast_results(os.path.join(os.getcwd(), self.path, filename))
    #     else:
    #         print("File or directory doesn't exist.\nPlease enter an existing"
    #               " file or directory and use the right option")
    #         return 1

    # def filter_and_sort_blast_results(self, path):
    #     """Call sub-processes to process the blast output files"""
    #     # Get filename with path excluded
    #     filename = path.split("/")[-1]
    #     if not os.path.exists("output/"):
    #         os.makedirs("output/")
    #     # Open file and call awk in subprocess to remove hits smaller than 24
    #     with open("output/"+filename.split(".")[0] + "_out1." + filename.split(".")[1], 'w') as output_1:
    #         subprocess.call(['awk', '$4>=24', path], stdout=output_1)
    #     # Open output file of awk and sort with a subprocess call
    #     with open("output/"+filename.split(".")[0] + "_out2." + filename.split(".")[1], 'w') as output_2:
    #         subprocess.call(['sort', '-k1,1', '-k3,3', output_1.name], stdout=output_2)
    #     os.remove(output_1.name)
    #     self.one_hit_blast_results(output_2.name)

    def one_hit_blast_results(self, filename):
        """Method to get all oligos with one blast hit from the results"""
        print(filename)
        previous_oligo = None
        previous_oligo_count = None
        self.one_hit = []
        count = 0
        with open(filename, 'r') as test:
            for line in test:
                count += 1
                line = line.split('\t')
                # Process data of first oligo
                if int(line[4]) == 0 and float(line[2]) >= 95.00 and eval(line[10]) < 0.0001:
                    if previous_oligo is None and previous_oligo_count is None:
                        previous_oligo = line
                        previous_oligo_count = 0

                    # Save one hit of the oligo in a list
                    if line[0] != previous_oligo[0] and previous_oligo_count == 1:
                        self.one_hit.append(previous_oligo)
                        previous_oligo = line
                        previous_oligo_count = 1
                    # Don't save the oligo hits
                    elif line[0] != previous_oligo[0] and previous_oligo_count != 1:
                        previous_oligo = line
                        previous_oligo_count = 1
                    else:
                        previous_oligo_count += 1
        print("Number of potential probes:", len(self.one_hit))

        self.to_database()
        #os.remove(filename)

    # def make_connection(self):
    #     """Makes connection with MySQL database. User data is obtained from cnf file"""
    #     config = configparser.ConfigParser()
    #     # Read my.cnf file.
    #     config.read('my.cnf')
    #     # Get username, password, database and hostname from hidden file.
    #     username = config.get('client', 'user')
    #     password_user = config.get('client', 'password')
    #     database_user = config.get('client', 'database')
    #     hostname = config.get('client', 'host')
    #     # Make connection to database.
    #     self.conn = mysql.connector.connect(host=hostname,
    #                                         database=database_user,
    #                                         user=username,
    #                                         password=password_user)

    def to_database(self):
        """Make connection to database and place complement sequence of the oligo in the database as a probe sequence"""
        #self.make_connection()
        conn = mysql.connector.connect(host=snakemake.config["database_host"],
                                            database=snakemake.config["database_name"],
                                            user=snakemake.config["database_username"],
                                            password=snakemake.config["database_password"])
        self.dict_probe_sequence = {}
        cursor = conn.cursor()
        for oligo in self.one_hit:
            oligo = oligo[0].replace("oligo_", "").replace(",", "")
            # Query to obtain information from database
            query = (
            "SELECT sequence FROM oligos WHERE ID={};".format(oligo))
            # Executing query
            cursor.execute(query)
            for result in cursor:
                self.dict_probe_sequence.update({result[0]: int(oligo)})
        cursor.close()
        nucleotiden = {"a": "t", "c": "g", "g": "c", "t": "a", "n": "n"}
        cursor = conn.cursor()
        for sequence_unique in self.dict_probe_sequence.keys():
            # Create complement sequence for unique probe
            complement = "".join([nucleotiden[x] for x in sequence_unique])
            # Call stored procedure to update database with information
            cursor.callproc('sp_set_boolean_set_complement_sequence',
                            args=(self.dict_probe_sequence[sequence_unique], complement))
            conn.commit()
        cursor.close()
        conn.close()








    # def one_hit_blast_results_to_file(self, filename):
    #     """Method to get all oligos with one blast hit from the results"""
    #     print(filename)
    #     previous_oligo = None
    #     previous_oligo_count = None
    #     self.one_hit = []
    #
    #     conn = mysql.connector.connect(host=snakemake.config["database_host"],
    #                                         database=snakemake.config["database_name"],
    #                                         user=snakemake.config["database_username"],
    #                                         password=snakemake.config["database_password"])
    #
    #
    #     count = 0
    #     with open(filename, 'r') as blastResultFile:
    #         for line in blastResultFile:
    #             count += 1
    #             line = line.split('\t')
    #             # Process data of first oligo
    #             if int(line[4]) == 0 and float(line[2]) >= 95.00 and eval(line[10]) < 0.0001:
    #                 if previous_oligo is None and previous_oligo_count is None:
    #                     previous_oligo = line
    #                     previous_oligo_count = 0
    #
    #                 # Save one hit of the oligo in a list
    #                 if line[0] != previous_oligo[0] and previous_oligo_count == 1:
    #                     self.one_hit.append(previous_oligo)
    #                     previous_oligo = line
    #                     previous_oligo_count = 1
    #                 # Don't save the oligo hits
    #                 elif line[0] != previous_oligo[0] and previous_oligo_count != 1:
    #                     previous_oligo = line
    #                     previous_oligo_count = 1
    #                 else:
    #                     previous_oligo_count += 1
    #     blastResultFile.close()
    #
    #     nucleotiden = {"a": "t", "c": "g", "g": "c", "t": "a", "n": "n"}
    #
    #     with open("updateDatabase.sql", "w") as updateDatabase:
    #         cursor = conn.cursor()
    #         for oligo in self.one_hit:
    #             oligo = oligo[0].replace("oligo_", "").replace(",", "")
    #             # Query to obtain information from database
    #             query = ("SELECT sequence FROM oligos WHERE id={};".format(oligo))
    #             # Executing query
    #             cursor.execute(query)
    #             for result in cursor:
    #                 complement = "".join([nucleotiden[x] for x in result[0]])
    #                 updateDatabase.write(("CALL sp_set_boolean_set_complement_sequence({}, '{}');\n".format(oligo, complement)))
    #         cursor.close()
    #     updateDatabase.close()
    #     print("Number of potential probes:", len(self.one_hit))
    #
    #     with open("out2.txt", "w") as bla:
    #         if len(self.one_hit) > 2:
    #             bla.write("Testerdetest")
    #     bla.close()


def main():
    start = time.time()
    Blastprocessor()
    print(time.time() - start)
    return 0


if __name__ == '__main__':
    main()
