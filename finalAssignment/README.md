# Snakemake workflow to create microarrays for Plasmodium Falciparum

Author: Kimberley Kalkhoven [k.l.kalkhoven@st.hanze.nl]
Copyright (c) 2018, all rights reserved.

## Introduction
A pipeline has been developed to create microarrays of Plasmodium falciparum.
The aim of the research is to develop the best microarray (varying in incubation temperature). In the pipeline, 7 microarray's are developed. The first has an incubation temperature of 51 celsius degrees and the last one a temperature of 57 degrees celsius. The results are visualized so that it can be made more transparent.

## Summary of workflow
The workflow starts with the acquisition of the genbank files from Plasmodium Falciparum. This is optional, you can also immediately start with files that the user already owns (also added in this repository). All GenBank files are parsed with a self written Python script. The script produces 2 output directories. The first directory "bulk" contains all data that is later loaded into the database. The second directory contains files for each chromosome with all the oligos.

After this, the workflow splits. First the database is set up and the stored procedures are loaded. After that, a blast database is created and a BLASTn process is started in which the oligos are compared against a reference. From this, the hits where only one location is found are filtered out and processed in the database.

Now that everything is in the database, several calculations are being made to create microarrays and to select the best from them. The results are visualized in 2 graphs.

An image of a DAY schedule of the workflow can be found in the source.

## Locations of all data

- All data that is not produced during the workflow is included in the repository.
- The data can also be found in: `/commons/student/2017-2018/Thema11/klkalkhoven/data`.
- Data locations where results are stored can be found in the 2 configuration files, the main results are stored in directories from the working directory.
- **It is important that when paths in the one configuration file are changed, this also happens in the second configuration file!**

## How to use the workflow?
- Make sure you download or copy all necessary data files for the workflow (place it in the working directory).
- Make sure the Snakefile is present in the working directory and Snakemake is installed.
- Now the workflow can be started with the commando: `snakemake --cores 8`.
- If you do not have 8 cores at your disposal, you can also enter a lower number. Remember that the execution of the workflow will take longer with a lower number.

Self tested differences with 1 core and 8 cores ( on our local computers at the study):
>**Number of cores:** 8
>**Time in which the total workflow is executed:** 50 minutes

>**Number of cores:** 1
>**Time in which the total workflow is executed:** 2 hours and 20 minutes

## Number of jobs for total workflow
```
Job counts:
	count	jobs
	1	all
	15	awkRemoveHitsShorterThan24
	1	createBlastDatabaseAndCopyToGridComputers
	1	createDatabaseStoredProcedures
	1	createDatabaseTables
	1	createMatrices
	1	createReport
	1	downloadFromNCBI
	1	getMatricesByQuality
	1	importDataToDatabase
	1	processBlastResults
	1	runBlastn
	1	showMicroarrayResults
	15	sortOnColumn1And3
	42
```

## Explanation of the workflow step by step

The workflow starts with one of the two rules below (dependent of your own preferences):
`Rule dowloadFromNCBI`
Download all the genbankfiles of Plasmodium falciparum and parse the genbank files to produce useful output.

`Rule parseGenBankFiles`
Parse all the genbankfiles of Plasmodium falciparum from a local directory and produce useful output.

After this, the workflow is simply followed.

`Rule createDatabaseTables`
Create all database tables needed for creating microarrays of Plasmodium falciparum.

`Rule createDatabaseStoredProcedures`
Create all stored procedures needed to create microarrays and to filter the best microarray.

`Rule importDataToDatabase`
Bulk process where the data obtained from the genbank parser is imported into the database.

`Rule createBlastDatabaseAndCopyToGridComputers`
Creates a BLAST database of the genome of Plasmodium falciparum and copy the database to all computers which belong to the grid.

`Rule runBlastn`
BLASTn is a very resource intensive process, since a grid of computers is available the calculations can be spread out on this grid with the help of condor. This decreases the execution time significantly without affecting the results.

`Rule awkRemoveHitsShorterThan24`
Commandline process to filter the blast results (.csv). All hits with a sequence shorter than 24 are removed. A new file is created.

`Rule sortOnColumn1And3`
Commandline process to sort the blast results (.csv) on column 1 and 3 after the filter process step. The results are stored in a new file.

`Rule processBlastResults`
Process all blast results to oligos/probes with one hit. These oligos are mostly valid probes. The probes are updated to the database (oligos table).

`Rule createMatrices`
Creates 7 matrices varying between 51 celsius degrees and 57 celsius degrees. All matching probes are linked to the microarray.

`Rule getMatricesByQuality`
Sort all microarrays on quality. The best microarray has the highest number of genes with one or more probes.

`Rule showMicroarrayResults`
Make visualization of the results. A graph with two lines, one for the number of genes with one or more probes and one for the number of genes without a probe. The second graph shows the average number of probes per gene.

`Rule createReport`
Make a short HTML page with the results (two figures and tab separated file) and a short description of the research.

