SET autocommit = 0;
SET foreign_key_checks = 0;

LOAD DATA LOCAL INFILE 'bulk/bulk_organisms.txt' INTO TABLE organisms
FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n';

LOAD DATA LOCAL INFILE 'bulk/bulk_chromosomes.txt' INTO TABLE chromosomes
FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n';

LOAD DATA LOCAL INFILE 'bulk/bulk_genes.txt' INTO TABLE genes
FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n';

LOAD DATA LOCAL INFILE 'bulk/bulk_oligos.txt' INTO TABLE oligos
FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n';

LOAD DATA LOCAL INFILE 'bulk/bulk_cds.txt' INTO TABLE cds
FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n';

LOAD DATA LOCAL INFILE 'bulk/bulk_start_stop.txt' INTO TABLE start_stop
FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n';

SET foreign_key_checks = 1;
SET autocommit = 1;

-- Create index for id and sequence oligos.
CREATE INDEX index_sequence ON oligos(sequence);
CREATE INDEX index_melt_temp ON oligos(melt_temp);
CREATE INDEX index_start ON oligos(start);