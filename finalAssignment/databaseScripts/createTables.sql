-- ----------------------------- DROP TABLES ----------------------------------
DROP TABLE IF EXISTS locations;
DROP TABLE IF EXISTS microarrays;
DROP TABLE IF EXISTS oligos;
DROP TABLE IF EXISTS start_stop;
DROP TABLE IF EXISTS cds;
DROP TABLE IF EXISTS genes;
DROP TABLE IF EXISTS chromosomes;
DROP TABLE IF EXISTS organisms;

-- ----------------------------- CREATE TABLES --------------------------------
-- Create table organisms.
CREATE TABLE organisms
(
  id   INT          NOT NULL    AUTO_INCREMENT,
  naam VARCHAR(100) NOT NULL    UNIQUE,
  PRIMARY KEY (id)
);

-- Create table chromosomes.
CREATE TABLE chromosomes
(
  chromosome_number VARCHAR(30)   NOT NULL,
  organism_name     VARCHAR(100) NOT NULL,
  PRIMARY KEY (chromosome_number, organism_name),
  FOREIGN KEY (organism_name)
  REFERENCES organisms (naam)
);

-- Create table genes.
CREATE TABLE genes
(
  id            INT           NOT NULL,
  chromosome_id varchar(30)   NOT NULL,
  organism_name VARCHAR(100)  NOT NULL,
  name          VARCHAR(50)   NOT NULL,
  start         INT           NOT NULL,
  stop          INT           NOT NULL,
  strand        CHAR(1)       NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (chromosome_id)
  REFERENCES chromosomes (chromosome_number)
);

-- Create table cds.
CREATE TABLE cds
(
  proteinID VARCHAR(50) NOT NULL,
  geneID    INT NOT NULL,
  product   TINYTEXT,
  PRIMARY KEY (proteinID),
  FOREIGN KEY (geneID) REFERENCES genes (id)
);

-- Create table start_stop.
CREATE TABLE start_stop
(
  ID    INT NOT NULL AUTO_INCREMENT,
  proteinID    VARCHAR(50) NOT NULL,
  start INT NOT NULL,
  stop  INT NOT NULL,
  PRIMARY KEY (ID),
  FOREIGN KEY (proteinID)
  REFERENCES cds (proteinID)
);

-- Create table oligos.
CREATE TABLE oligos
(
  id             INT      NOT NULL    AUTO_INCREMENT,
  gene_id        INT      NOT NULL,
  gc_percentage  INT      NOT NULL,
  melt_temp      FLOAT    NOT NULL,
  sequence       CHAR(25) NOT NULL,
  start          INT      NOT NULL,
  probe          TINYINT(1)           DEFAULT NULL,
  probe_sequence CHAR(25)             DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (gene_id)
  REFERENCES genes (id)
);

-- Create table microarrays.
CREATE TABLE microarrays
(
  id           INT AUTO_INCREMENT,
  incubat_temp INT NOT NULL,
  PRIMARY KEY (id)
);

-- Create table locations.
CREATE TABLE locations
(
  microarray_id INT NOT NULL,
  probe_id      INT NOT NULL,
  id            INT NULL,
  PRIMARY KEY (microarray_id, probe_id),
  FOREIGN KEY (microarray_id)
  REFERENCES microarrays (id),
  FOREIGN KEY (probe_id)
  REFERENCES oligos (id)
);
