-- ----------------------------- STORED PROCEDURES ----------------------------------
-- Drop all procedures if exists.
-- DROP PROCEDURE IF EXISTS sp_get_genes;
-- DROP PROCEDURE IF EXISTS sp_get_tm_vs_probes;
-- DROP PROCEDURE IF EXISTS sp_mark_duplicate_oligos;
DROP PROCEDURE IF EXISTS sp_create_matrix;
-- DROP PROCEDURE IF EXISTS sp_get_oligos_by_tm;
DROP PROCEDURE IF EXISTS sp_create_probe;
-- DROP PROCEDURE IF EXISTS sp_remove_overlapping_probes_from_matrix;
DROP PROCEDURE IF EXISTS sp_get_matrices_by_quality;
DROP PROCEDURE IF EXISTS sp_set_boolean_set_complement_sequence;

-- Set delimiter to //.
DELIMITER //

-- Procedure to set boolean unique oligo and add complement sequence
CREATE PROCEDURE sp_set_boolean_set_complement_sequence(IN input_ID INT, IN input_probe_sequence CHAR(25))
  BEGIN
    UPDATE oligos SET probe = 1, probe_sequence = input_probe_sequence WHERE ID=input_ID;
  END;
//


-- Procedure to create a probe.
CREATE PROCEDURE sp_create_probe(IN matrix_id INT, IN oligo_id INT)
  BEGIN
    INSERT INTO locations (microarray_id, probe_id)
    VALUES (matrix_id, oligo_id);
  END;
//

-- Procedure to create a microarray.
CREATE PROCEDURE sp_create_matrix(IN melting_t FLOAT, IN max_difference FLOAT)
  BEGIN
    DECLARE done BOOLEAN DEFAULT FALSE;
    DECLARE cursor_val INT;
    DECLARE microarray_ID INT;
    DECLARE probes CURSOR FOR SELECT id
                              FROM oligos
                              WHERE
                                melt_temp BETWEEN melting_t - max_difference AND
                                melting_t + max_difference AND probe = 1;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    INSERT INTO microarrays (incubat_temp) VALUES (melting_t);
    SELECT LAST_INSERT_ID()
    INTO microarray_ID;
    OPEN probes;
    readloop: LOOP
      FETCH probes
      INTO cursor_val;
      IF done
      THEN
        LEAVE readloop;
      END IF;
      CALL sp_create_probe(microarray_ID, cursor_val);
    END LOOP readloop;
    CLOSE probes;
  END;
//

-- Procedure to get all matrices by quality.
CREATE PROCEDURE sp_get_matrices_by_quality()
  BEGIN
    DECLARE total_genes INT DEFAULT NULL;
    SELECT COUNT(*)
    FROM genes
    INTO total_genes;
    SELECT
      l.microarray_id,
      m.incubat_temp,
      COUNT(DISTINCT o.gene_id)                     AS 'Genes 1 or more probes',
      total_genes - COUNT(DISTINCT o.gene_id)          'Genes no probe',
      COUNT(l.probe_id) / COUNT(DISTINCT o.gene_id) AS "Average number of probes per gene"
    FROM oligos o INNER JOIN locations l ON l.probe_id = o.id INNER JOIN microarrays m on m.id=l.microarray_id
    WHERE microarray_id IS NOT NULL
    GROUP BY l.microarray_id
    ORDER BY COUNT(DISTINCT o.gene_id) DESC;
  END;
//

-- Set delimiter to ;.
DELIMITER ;