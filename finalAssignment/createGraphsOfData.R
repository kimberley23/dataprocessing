# setwd("/Users/kimberley/Documents/Bio-informatica/Periode11/Dataprocessing/dataprocessing/finalAssignment/")
setwd(snakemake@input[[1]])

data <- as.matrix(read.table(snakemake@input[[2]], header=TRUE, sep="\t"))
data2 <- data[,3:5]
row.names(data2) <- data[,2]

# Data sorted on incubation temperature
# Rownames is temperature, cols are values of genes with 1 or more probes, genes without a probe,
# and the average number of probes per gene.
data2.sorted <- data2[order(rownames(data2)),]


# Make values easy to plot
x <- rownames(data2.sorted)
y1 <- data2.sorted[,1]
y2 <- data2.sorted[,2]
y3 <- data2.sorted[,3]

# First plot: showing genes with 1 or more probes (green) and genes without a probe (red)
png(snakemake@output[[1]], width=900)

par(mar=c(5,4,4,5)+.1)
plot(x,y1,type="b",col="green", 
     xlab=expression(paste("Incubation temperature (",degree,"C)")), 
     ylab="Genes with one or more probes", 
     pch=15,
     main="Distribution of genes on a microarray, specified in the number \n of genes with one or more probes and genes without a probe")
grid()

par(new=TRUE)
plot(x, y2,type="b",col="red",xaxt="n",yaxt="n",xlab="",ylab="", pch=16)
axis(4)

mtext("Genes with no probe",side=4,line=3)
legend("left",col=c("green","red"), pch=c(15:16),
       legend=c("Genes with one or more probes","Genes with no probe"))

dev.off()




# Second plot: showing average number of probes per gene
png(snakemake@output[[2]], width=900)

plot(x,y3,type="b",col="blue", 
     xlab=expression(paste("Incubation temperature (",degree,"C)")), 
     ylab="Average number of probes per gene", 
     pch=15,
     main = "Average number of probes per gene on a microarray, \n microarrays are created with different incubation temperatures")
grid()

legend("topright",col=c("blue"),pch=15,legend="Average number of probes per gene")

dev.off()

