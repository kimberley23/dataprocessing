#!/usr/bin/env python3


class Gene:
    """Gene class for storing all the information for a specefic gene, also
    contains the corresponding CDS object"""
    def __init__(self, gene_id, location, gene_name):
        self.gene_id = gene_id
        self.name = gene_name
        self.start = 0
        self.stop = 0
        self.sequence = ''
        self.strand = ''
        self.location = location
        self.cds = None

    def get_coding_region(self):
        """Get the required information for the CDS and add this data to the
        CDS object"""
        # If the gene has a cds
        if self.cds is not None:
            # For every location in the cds locations list
            for num in self.cds.location:
                # Add the specified location to the cds sequence (the exons)
                if len(num) == 1:
                    self.cds.sequence += "".join(self.sequence[int(num[0]) - 1 - self.start])
                else:
                    self.cds.sequence += "".join(self.sequence[int(num[0]) - 1 - self.start:int(num[1]) - self.start])
                    # If gene is complement reverse the cds and gene sequence
            if self.strand == 'complement':
                self.strand = 'C'
                self.cds.sequence = self.cds.sequence[::-1]
                self.sequence = self.sequence[::-1]
            else:
                self.strand = 'W'
            # Create the oligos for the CDS
            self.cds.create_oligos()
