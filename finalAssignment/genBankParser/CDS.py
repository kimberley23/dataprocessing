#!/usr/bin/env python3
from Oligo import Oligo


class CDS:
    """Class for storing CDS data, also contains all possible oligos on the cds
    in a list"""
    def __init__(self, location, name, product, gi, id, protein_id):
        self.name = name
        self.gi = gi
        self.id = id
        self.protein_id = protein_id
        self.location = [number.split('..') for number in location if
                         number != '']
        self.sequence = ''
        self.oligos = []
        self.product = product

    def create_oligos(self):
        """Calculate all possible oligos for this CDS"""
        # Get every possible dna sequence of 25 nucleotides
        for x in range(len(self.sequence)):
            sequence = self.sequence[x:x+25]
            # If sequence is 25 nucleotides
            if len(sequence) == 25:
                # Create oligo object with 25 nucleotides as sequence
                oligo = Oligo(sequence, x, x+25)
                # If oligo matches all requirements add to the list
                if oligo.calc_gc_percentage() and oligo.calc_baseruns() and oligo.calc_dinucleotides() and oligo.calc_tm() and oligo.calc_hairpin():
                    self.oligos.append(oligo)
