#!/usr/bin/env python3
""""
Name: genbank_parser.py
Purpose: Processing a genbank file and calculating possible oligo nucleotides
Commandline usage: genbank_parser.py [-h] -d <path to genbank file or directory>
Author: Sven Hakvoort & Kimberley Kalkhoven
Created: 25/11/2016
Version: 1.0
"""
import sys
import time
import re
import os
from Gene import Gene
from Organism import Organism
from CDS import CDS
from Chromosome import Chromosome
from multiprocessing import Manager, Pool


class GenbankParser:
    """Class for parsing one or multiple genbank files and storing relevant
    data. Stores all the chromosomes in objects, those objects then contain gene
    objects, which contain a CDS object. The CDS contains Oligo classes for
    every possible oligo"""
    # Temporary variables for storing data for later use
    manager = Manager()
    find_cds = False
    find_gene = False
    find_loc = False
    chromosome = ''
    current_gene_name = ''
    current_gene_location = ''
    current_gene_id = ''
    organism = manager.list()
    translation = ''
    protein_id = ''
    cds_name = ''
    cds_gi = ''
    cds_location = ''
    product = ''
    # List in which all chromosome objects are stored
    chromosomes = manager.list()

    def __init__(self):
        self.filename = ''
        self.get_files()
        self.output_bulk(self.chromosomes)
        self.output_multifasta_oligos(self.chromosomes)

    def get_files(self):
        filenames = []

        p = Pool(snakemake.threads)
        
        for genbankFile in snakemake.input:
            filenames.append(os.path.join(os.getcwd(), genbankFile))
            # Join the path to get the full path to the file
        p.map(self.read_file, filenames)
        
        # Enter number of cores to use
        #p = Pool(4)
        #"""Function for getting all the file names in a dir, determines if
        #self.filename is a file or a directory. If it is a directory it lists
        #all the files and calls the self.read_file function for every file.
        #If self.filename is a single file it calls self.read_file just once"""
        #if os.path.isfile(self.filename):
        #    self.read_file(self.filename)
        #elif os.path.isdir(os.path.join(os.getcwd(), self.filename)):
        #    for filename in os.listdir(self.filename):
        #        filenames.append(os.path.join(os.getcwd(), self.filename,
        #                                      filename))
        #        # Join the path to get the full path to the file
        #    p.map(self.read_file, filenames)
        #else:
        #    print("File or directory doesn't exist.\nPlease enter an existing"
        #          " file or directory and use the right option")
        #    return 2

    def read_file(self, filename):
        """Function for reading a genbank file, processing every line, looking
        for the required data. Calls other function to check and process the
        data"""
        # If file doesn't correspond with genbank characteristics
        if filename.split('.')[-1] != 'gb' and filename.split('.')[1] != 'gbk':
            print("Error: {} is not a genbank file or doesn't have the right "
                  "extension name.\nPlease use a valid genbank "
                  "file".format(filename))
            return 1
        try:
            with open(filename, 'r') as opened:
                chromosome = ''
                print("Processing", filename)
                for line in opened:
                    line = line.split()
                    if line:
                        # Find chromosome entry
                        chromosome = self.get_chromosome(line, chromosome)
                        # Find gene entry's
                        self.get_gene(line, chromosome)
                        # Find CDS entry's
                        self.get_cds(line, chromosome)
                        if line[0] == 'ORGANISM':
                            self.organism.append(Organism(' '.join(line[1:])))
                        # If the genome sequence is found store it
                        if line[0] == 'ORIGIN':
                            sequence = opened.readlines()
                            # Format the origin, so one string is created with the
                            # whole genome
                            chromosome.sequence = "".join(["".join(line.split()[1:])
                                     for line in sequence if
                                     re.search("[actg]", line)])
                            # Let the chromosome object calculate the sequences for
                            # each gene
                            chromosome.get_gene_sequences()
                self.chromosomes.append(chromosome)
        except IOError:
            print("Error: No permission to read the file.\nPlease make sure "
                  "you have read access to the given file")
            return 3

    def get_chromosome(self, line, chromosome):
        """Function for finding chromosome info, stores this in a chromosome
        object, which is stored in a list. Also keeps track of which chromosome
        is currently being processed"""
        if line[0].startswith('/chromosome=') or line[0].startswith('/organelle'):
            # Get chromosome id
            id_num = line[0][line[0].index('=') + 2:-1]
            # Store current chromosome object in global variable
            chromosome = Chromosome(id_num)
            # Add chromosome object to all chromosomes
        return chromosome

    def get_gene(self, line, chromosome):
        """Function for getting all the data of genes and storing this in a gene
         object, this object is then stored in the current chromosome object"""
        check = ['mRNA', 'rRNA', 'misc_feature', 'ORIGIN', 'gene', 'CDS']
        # A gene entry is found, store the id and keep looking for the
        # rest of the data
        if line[0] == 'gene':
            self.current_gene_location = line[1]
            self.find_gene = True
        elif line[0] in check and self.find_gene is True:
            # Create gene object and append to the parser
            chromosome.genes.update(
                    {self.current_gene_id: Gene(self.current_gene_id,
                                                self.current_gene_location,
                                                self.current_gene_name)})
            # Stop searching for a gene and reset gene name
            self.current_gene_name = ''
            self.find_gene = False
        # If the gene name or locus tag is found store this data
        # Only store locus tag if gene name is empty
        elif (line[0].startswith('/gene=') and self.find_gene is True) or (
                        line[0].startswith('/locus_tag=') and self.find_gene is True and
                        self.current_gene_name == ''):
            line = " ".join(line)
            self.current_gene_name = line[line.index('=') + 1:].strip('"')
        # Look for an id and store this value
        elif line[0].startswith('/db_xref=') and self.find_gene is True:
            # Get the location of the gene
            self.current_gene_id = line[0][line[0].index(':') + 1:-1]

    def get_cds(self, line, chromosome):
        """Function for getting data of every CDS, creating a CDS object and
        combining this with the corresponding gene"""
        if line[0] == 'CDS':
            # Turn on searching for CDS
            self.find_cds = True
            # Process the CDS line so the locations are placed in a list
            # remove all other data like special signs and complement/join
            for ch in ['(', ')', 'complement', 'join', '<', '>']:
                if ch in line[1]:
                    line[1] = line[1].replace(ch, '')
            self.cds_location = line[1].split(",")
            self.find_loc = True
        # If the next line contains more locations from the last line add these
        # to the list
        elif line[0].startswith('/product='):
            self.product = line[0][line[0].index('=') + 2:-1]
        elif line[0].startswith('/translation='):
            self.translation = line[0][line[0].index('=') + 2:-1]
        elif line[0].startswith('/protein_id='):
            self.protein_id = line[0][line[0].index('=') + 2:-1]
        # If the gene name or locus tag is found store this
        elif (line[0].startswith('/gene=') and self.find_cds is True) or (
                    line[0].startswith('/locus_tag=') and self.find_cds is True):
            line = " ".join(line)
            # Get the name or locus tag and store this
            self.cds_name = line[line.index('=') + 2:-1]
            self.find_loc = False
        elif line[0].startswith('/db_xref="GOA') and self.find_cds is True:
            # Get the location of the gene
            self.cds_gi = line[0][line[0].index(':') + 1:-1]
        elif line[0].startswith('/db_xref="GeneID') and self.find_cds is True:
            cds_id = line[0][line[0].index(':') + 1:-1]
            # If the CDS corresponds with a gene
            if cds_id in chromosome.genes:
                # Create cds object and add to the corresponding gene object
                chromosome.genes[cds_id].cds = CDS(self.cds_location,
                                                   self.cds_name, self.product,
                                                   self.cds_gi, cds_id,
                                                   self.protein_id)
                # Turn off searching for gene and reset cds name
                self.find_cds = False
        elif re.match("^[0-9]+", line[0]) and self.find_loc is True:
            self.cds_location += line[0].replace(')', '').replace("<", "").replace(">", "").split(',')

    def output_bulk(self, chromosomes):
        # Generating lines for output file as test
        bestanden = {'bulk_chromosomes': open(os.path.join(os.getcwd(), "bulk", 'bulk_chromosomes.txt'), 'w'),
                     'bulk_genes': open(os.path.join(os.getcwd(), "bulk", 'bulk_genes.txt'), 'w'),
                     'bulk_cds': open(os.path.join(os.getcwd(), "bulk", 'bulk_cds.txt'), 'w'),
                     'bulk_start_stop': open(os.path.join(os.getcwd(), "bulk", 'bulk_start_stop.txt'), 'w'),
                     'bulk_oligos': open(os.path.join(os.getcwd(), "bulk", 'bulk_oligos.txt'), 'w')}
        auto_increment_chromosome = 0
        chromosomes = sorted(chromosomes, key=lambda x: int(x.chromosome_id) if x.chromosome_id.isdigit() else 0)
        for item in chromosomes:
            # Counter for import database (so database has no auto_increment)
            auto_increment_chromosome += 1
            # Bulk import chromosomes table.
            bestanden['bulk_chromosomes'].write(
                "{};{};\n".format(item.chromosome_id, self.organism[0].name))
            for gene in sorted(item.genes):
                # Bulk import genes table.
                bestanden['bulk_genes'].write(
                        "{};{};{};{};{};{};{};\n".format(
                            item.genes[gene].gene_id,
                            item.chromosome_id,
                            self.organism[0].name,
                            item.genes[gene].name,
                            item.genes[gene].start, item.genes[gene].stop,
                            item.genes[gene].strand))
                # Bulk import CDS table.
                if item.genes[gene].cds is None:
                    pass
                else:
                    bestanden['bulk_cds'].write(
                            "{};{};{};\n".format(item.genes[gene].cds.protein_id,
                                                    item.genes[gene].cds.id,
                                                    item.genes[gene].cds.product,
                                                    ))
                    for locatie in item.genes[gene].cds.location:
                        # Bulk import start_stop table.
                        bestanden['bulk_start_stop'].write(
                            "\\N;{};{};{};\n".format(item.genes[gene].cds.protein_id,
                                                 locatie[0],
                                                 locatie[-1]))

                    for oligo in item.genes[gene].cds.oligos:
                        # Bulk import oligos table.
                        bestanden['bulk_oligos'].write(
                                "\\N;{};{};{};{};{};\\N;\\N;\n".format(
                                    item.genes[gene].cds.id,
                                    oligo.gc_percentage, oligo.tm,
                                    oligo.sequence, oligo.start))
        # Close all files.
        for bestand in bestanden.values():
            bestand.close()

    def output_multifasta_oligos(self, chromosomes):
        """
        Generating multifasta file containing all the oligos. The header contains an oligo number and GI number.
        """
        teller = 0
        chromosomes = sorted(chromosomes, key=lambda x: int(x.chromosome_id) if x.chromosome_id.isdigit() else 0)
        for item in chromosomes:
            lalala = 0

            if item.chromosome_id == 'mitochondrion':
                # Set number = 0 for file name.
                number = 0
            else:
                number = item.chromosome_id
            with open(os.path.join(os.getcwd(), "oligos", 'Oligos_chrom_{}.txt'.format(number)), 'w') as multifasta:
                for gene in sorted(item.genes):
                    if item.genes[gene].cds is None:
                        pass
                    else:
                        for oligo in item.genes[gene].cds.oligos:
                            teller += 1
                            lalala += 1
                            # Write multifasta lines
                            multifasta.write(
                                "> oligo_{}, proteinID:{}\n{}\n".format(teller,
                                                                 item.genes[
                                                                     gene].cds.protein_id,
                                                                 oligo.sequence))
            #print(number)
            #print(lalala)


def main():
    start = time.time()
    g = GenbankParser()
    print(time.time() - start)
    return 0


if __name__ == '__main__':
    sys.exit(main())
