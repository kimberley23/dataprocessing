#!/usr/bin/env python3
from itertools import product

# All possible baseruns which are too long
bases = ['aaaa', 'gggg', 'cccc', 'tttt']
# All possible dinucleotides which can form repeats
di_nucs = [x for x in
           ["".join(comb) * 3 for comb in list(product('acgt', repeat=2))]]
complement = {'a': 't', 'g': 'c', 'c': 'g', 't': 'a', 'n': 'n'}


class Oligo:
    """Oligo class, contains all the relevant data about a oligo"""

    def __init__(self, sequence, start, stop):
        self.sequence = sequence
        self.gc_percentage = 0
        self.tm = 0
        self.start = start
        self.stop = stop

    def calc_gc_percentage(self):
        """Calculate oligo gc content"""
        g = self.sequence.count('g')
        c = self.sequence.count('c')
        self.gc_percentage = (g + c) / len(
            self.sequence) * 100
        # If percentage between 10% and 50% oligo is valid
        if 10 <= self.gc_percentage <= 50:
            return True
        else:
            return False

    def calc_baseruns(self):
        """Function for determining if a sequence has baseruns of more than 3
        nucleotides"""
        baseruns = [True if self.sequence.find(base) == -1 else False for base
                    in bases]
        # If a baserun of more than 3 nucleotides if found, oligo is not valid
        if False in baseruns:
            return False
        else:
            return True

    def calc_dinucleotides(self):
        """Function for determining if a sequence has more than 2 consecutive
        dinucleotide repeats"""
        di_nucleotides = [True if self.sequence.find(di_nuc) == -1 else False
                          for di_nuc in di_nucs]
        # If a dinucleotide repeat longer than 2 consecutive dinucleotides,
        # oligo is not valid
        if False in di_nucleotides:
            return False
        else:
            return True

    def calc_tm(self):
        """Calculte melting temperature with the wallace formula"""
        gc = len(self.sequence) / 100 * self.gc_percentage
        at = len(self.sequence) - gc
        self.tm = 64.9 + 41 * (gc - 16.4) / (at + gc)
        # If tm between 50 degrees celsius and 70 degrees celsius, oligo is
        # valid
        if 50 <= self.tm <= 70:
            return True
        else:
            return False

    def calc_hairpin(self):
        """Determine if oligo can form a hairpin structure"""
        distance = 0
        # For length of sequence - 13 because minimal required length for a
        # hairpin is 13. When the sequence has less than 13 characters left, it
        # is not possible a hairpin wil form.
        for x in range(len(self.sequence) - 13):
            # Take every possible piece of 5 nucleotides from the sequence
            check = self.sequence[x:x + 5]
            # Check if length is 5
            if len(check) == 5:
                # Determine if the check has a reversed sequence in the strand
                reversed_check = "".join([base.replace(base, complement[base])
                                          for base in check][::-1])
                index = self.sequence.find(reversed_check)
                # If reversed check is found, determine distance between the
                # two pieces
                if index != -1:
                    if index > x + 5:
                        distance = index - (x + 5)
                    # If distance is longer than 3 a hairpin can be formed,
                    # oligo is not valid
                    if distance >= 3:
                        return False
        return True
