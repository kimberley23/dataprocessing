#!/usr/bin/env python3
complement = {'a': 't', 'g': 'c', 'c': 'g', 't': 'a', 'n': 'n'}


class Chromosome:
    """Chromosome class, used for storing data about a chromosome and store
    all gene objects in a dict with the gene name as key. Also store the whole
    chromosome dna sequence in self.sequence"""
    def __init__(self, chromosome_id):
        self.chromosome_id = chromosome_id
        self.genes = {}
        self.sequence = ''

    def get_gene_sequences(self):
        """Get the dna sequence for every gene on the chromosome"""
        for gene in self.genes.values():
            # Strip possible signs from the location variable
            start_stop = gene.location.replace("<", "").replace(
                    ">", "").split('..')
            # If the gene is on the complementary strand strip excessive data
            # and get complement strand for gene
            if start_stop[0].startswith('complement'):
                start_stop = [start_stop[0].strip('complement('),
                              start_stop[1].strip(')')]
                gene.strand = 'complement'
                # -1 because human indexing is different from code index
                gene.start = int(start_stop[0]) - 1
                gene.stop = int(start_stop[1])
                # Convert sequence to complement DNA
                gene.sequence = "".join(
                        [base.replace(base, complement[base]) for base in
                         self.sequence[
                         int(start_stop[0]) - 1:int(start_stop[1])]])
            else:
                # -1 because human indexing is different from code index
                gene.start = int(start_stop[0]) - 1
                gene.stop = int(start_stop[1])
                gene.sequence = self.sequence[
                                int(start_stop[0]) - 1:int(start_stop[1])]
            # Get CDS for every gene
            gene.get_coding_region()
