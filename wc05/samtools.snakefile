rule samtools_sort:
    input: 
        "mapped_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam"
    message:
        "Sort the mapped reads of {input} to sorted reads {output}"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"


rule sametools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    message:
        "Create an index for the following {input} to generate an indexed file {output}"
    shell:
        "samtools index {input}"
