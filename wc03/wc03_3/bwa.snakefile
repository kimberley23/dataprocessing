rule bwa_map:
    input:
        config["genome"] + config["ext"],
        config["fastqdir"] + "{sample}.fastq"
    output:
        temp("mapped_reads/{sample}.bam")
    message: 
        "Executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem {input} | samtools view -Sb > {output}"
